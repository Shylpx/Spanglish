class PagesController < ApplicationController
  include HTTParty
  
  skip_before_action :authenticate_user!, only: :home
  
  def home
  end

  def search
  end
  
  def translate
    response = JSON.parse HTTParty.get(
      Rails.application.secrets.yandex['uri'],
      {
        query: {
          key: Rails.application.secrets.yandex['api_key'],
          lang: "#{params['source']}-#{params['target']}",
          text: params['text']
        }
      }
    ).body
    render json: { code: response['code'], translation: response.try(:[], 'text').try(:first) }
  end
end

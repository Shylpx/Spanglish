class WordsController < ApplicationController

  before_action :get_word, :check_word_access, only: [:show, :edit, :update, :destroy]

  def new
    @word = Word.new
    @user_word = params[:w].present? ? params[:w] : ''
    @suggested_translation = params[:t].present? ? params[:t] : ''
  end

  def create
    @word = Word.new(word_params.merge({'user_id' => current_user.id}))
    if @word.save
      flash[:success] = "Added the word '#{@word.word}'"
      redirect_to add_word_path
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @word.update_attributes(word_params)
      flash[:success] = "Edited the word '#{@word.word}'"
      redirect_to edit_word_path(id: @word.id)
    else
      render :edit
    end
  end

  def destroy
    if @word.destroy
      flash[:success] = "Deleted the word '#{@word.word}'"
      redirect_to words_path(q: params[:q], n_words: params[:n_words])
    else
      render :index
    end
  end

  def index
    @search = current_user.words.ransack(params[:q])
    @words = @search.result.order('lower(word) ASC').page(params[:page]).per(params[:n_words] || 25)
  end

  private
    def get_word
      @word = Word.find_by_id params[:id]
    end

    def check_word_access
      if !@word.present? || @word.user != current_user
        flash[:warning] = "Access denied: sorry, you don't have access to this page"
        redirect_to root_path
      end
    end

    def word_params
      params.require(:word).permit(:word, :translation, :definition)
    end
end

module ApplicationHelper

  def page_title ext
    base = "Spanglish"
    if ext.empty?
      base
    else
      "#{base} | #{ext}"
    end
  end
  
  def active_class(*actions)
    actions.include?("#{controller_name}##{action_name}") ? "active" : ""
  end

end

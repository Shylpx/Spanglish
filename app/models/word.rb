class Word < ActiveRecord::Base
  
  validates :word, presence: true, length: {maximum: 255}, uniqueness: { scope: :user_id, case_sensitive: false }
  validates :translation, presence: true, length: {maximum: 255}
  validates :definition, length: {maximum: 255}
  validates :user_id, presence: true
  
  belongs_to :user
  
end

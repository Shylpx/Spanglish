
ENTER_KEY_CODE = 13
SEARCH_TIMEOUT = 10000

SOURCE_LANG = 'en'
TARGET_LANG = 'es'

endTime = (time) ->
  return if time['end']
  time['end'] = true
  $('#spinner').removeClass 'glyphicon glyphicon-refresh glyphicon-spin'
  pageAlert 'Error: An error has ocurred, please try again later', 'danger'
  return

$(document).ready ->
  
  $('#word').keyup (e) ->
    if e.keyCode == ENTER_KEY_CODE
      $('#search_word').click()
    return
  
  $('#search_word').click (e) ->
    clearAlerts()
    if document.getElementById('word').value == ""
      pageAlert "Word can't be blank", 'danger'
      return
    $('#spinner').addClass 'glyphicon glyphicon-refresh glyphicon-spin'
    time = end: false
    setTimeout endTime, SEARCH_TIMEOUT, time
    $.ajax
      type: 'GET'
      url: $('#translate_path').val()
      data:
        source: SOURCE_LANG
        target: TARGET_LANG
        text: $('#word').val()
      success: (response) ->
        return if time['end']
        time['end'] = true
        $('#translated_word').text(response.translation)
        $('#spinner').removeClass('glyphicon glyphicon-refresh glyphicon-spin')
        return
    return
  
  $('#add_word').click (e) ->
    e.preventDefault()
    window.location = $('#add_word_path').val() + '?w=' + $('#word').val() + '&t=' + $('#translated_word').text()
    return
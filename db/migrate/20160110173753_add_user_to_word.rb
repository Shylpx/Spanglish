class AddUserToWord < ActiveRecord::Migration
  def change
    add_reference :words, :user, index: true, foreign_key: true
    add_index :words, [:user_id, :created_at]
  end
end
